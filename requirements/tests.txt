#
# This file is autogenerated by pip-compile with Python 3.8
# by the following command:
#
#    pip-compile --config=pyproject.toml --no-annotate --resolver=backtracking requirements/tests.in
#
astroid==2.15.6
bleach==6.0.0
cfgv==3.3.1
coverage[toml]==7.2.7
cython==0.29.36
dill==0.3.6
distlib==0.3.6
docutils==0.20.1
exceptiongroup==1.1.2
filelock==3.12.2
identify==2.5.24
iniconfig==2.0.0
isort==5.12.0
lazy-object-proxy==1.9.0
mccabe==0.7.0
nodeenv==1.8.0
packaging==23.1
platformdirs==3.8.1
pluggy==1.2.0
pre-commit==3.3.3
pygments==2.15.1
pylint==2.17.4
pylint-per-file-ignores==1.2.1
pytest==7.4.0
pytest-cov==4.1.0
pytest-repeat==0.9.1
pytest-timeout==2.1.0
pyyaml==6.0
readme-renderer==40.0
six==1.16.0
tomli==2.0.1
tomlkit==0.11.8
typing-extensions==4.7.1
virtualenv==20.24.0
webencodings==0.5.1
wrapt==1.15.0

# The following packages are considered to be unsafe in a requirements file:
# setuptools
