#
# This file is autogenerated by pip-compile with Python 3.8
# by the following command:
#
#    pip-compile --config=pyproject.toml --no-annotate --resolver=backtracking requirements/build.in
#
build==0.10.0
click==8.1.5
cython==0.29.36
delocate==0.10.4 ; sys_platform == "darwin"
packaging==23.1
pip-tools==7.0.0
pyproject-hooks==1.0.0
tomli==2.0.1
typing-extensions==4.7.1
wheel==0.40.0

# The following packages are considered to be unsafe in a requirements file:
# pip
# setuptools
